from django.contrib import admin
from .models import *

admin.site.register(BasicInfo)
admin.site.register(PassportInfo)
admin.site.register(ApplicationInfo)
admin.site.register(VisaInfo)
admin.site.register(LanguageProficiency)
admin.site.register(AcademicQualification)
admin.site.register(WorkExperience)
admin.site.register(TestScore)
admin.site.register(Disability)
admin.site.register(Scholarship)
admin.site.register(SponsorInfo)
admin.site.register(LocalGuardianInfo)
admin.site.register(Recommendations)
admin.site.register(PaymentInfo)
admin.site.register(FinalDeclaration)
admin.site.register(PaymentIntermediate)
