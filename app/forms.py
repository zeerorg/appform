from django.forms import ModelForm
from django import forms
from .models import *


class FormBasicInfo(ModelForm):
    class Meta:
        model = BasicInfo
        fields = [
            'first_name',
            'middle_name',
            'last_name',
            'user_pic',
            'gender',
            'date_of_birth',
            'parent_first_name',
            'parent_middle_name',
            'parent_last_name',
            'country_of_citizenship',
            'country_of_birth',
            'ethnic_group',
            'phone_number',
            'email',
            'permanent_address',
            'current_address'
        ]
        widgets = {
            'date_of_birth': forms.DateInput(attrs={'class':'datepicker'}),
        }
        labels = {
            'parent_first_name': "Parent's First Name",
            'parent_middle_name': "Parent's Middle Name",
            'parent_last_name': "Parent's Last Name",
            'user_pic': "Your Photo"
        }


class FormPassportInfo(ModelForm):
    class Meta:
        model = PassportInfo
        fields = [
            'passport_number',
            'place_of_issue',
            'issuing_authority',
            'date_of_issue',
            'passport_expiry_date'
        ]
        widgets = {
            'date_of_issue': forms.DateInput(attrs={'class': 'datepicker'}),
            'passport_expiry_date': forms.DateInput(attrs={'class': 'datepicker'}),
        }


class FormVisaInfo(ModelForm):
    class Meta:
        model = VisaInfo
        fields = [
            'applied_for_indian_visa',
            'visa_type',
            'visa_expiry_date',
            'immigration_office',
            'refused_entry',
        ]
        widgets = {
            'visa_expiry_date': forms.DateInput(attrs={'class': 'datepicker'}),
            'applied_for_indian_visa': forms.RadioSelect,
            'refused_entry': forms.RadioSelect
        }
        labels = {
            'applied_for_indian_visa': 'Have you applied for an Indian Visa OR do you hold an Indian Visa?',
            'refused_entry' : 'Have you ever been refused an entry visa to any country?'
        }


class FormApplicationInfo(ModelForm):
    class Meta:
        model = ApplicationInfo
        fields = [
            'program_applying_for',
            'department_preference_1',
            'department_preference_2',
            'department_preference_3',
            'department_preference_4',
            'phd_interest',
            'published_papers'
        ]
        widgets = {

        }

        labels = {
            'department_preference_1': 'Course Preference 1',
            'department_preference_2': 'Course Preference 2',
            'department_preference_3': 'Course Preference 3',
            'department_preference_4': 'Course Preference 4',
            'phd_interest': 'Field of Interest',
            'published_papers': 'Published Papers (optional)'
        }


class FormLanguageProficiency(ModelForm):
    class Meta:
        model = LanguageProficiency
        fields = [
            'proficient_english',
            'english_document',
            'toefl_choice',
            'toefl_score',
            'ielts_choice',
            'ielts_score',
            'other_languages_known'
        ]
        widgets = {
            'proficient_english': forms.RadioSelect,
            'toefl_choice': forms.RadioSelect,
            'ielts_choice': forms.RadioSelect,
        }
        labels = {
            'proficient_english': 'Are you proficient in English language?',
            'english_document': '(If Yes, please attach supporting documents for English proficiency)',
            'toefl_choice': 'TOEFL',
            'ielts_choice': 'IELTS',
            'toefl_score': 'TOEFL Score',
            'ielts_score': 'IELTS Score'
        }


class FormAcademicQualification(ModelForm):
    class Meta:
        model = AcademicQualification
        fields = [
            'degree1', 'institution1', 'institution_address1', 'cgpa_percentage1', 'marksheet1',
            'degree2', 'institution2', 'institution_address2', 'cgpa_percentage2', 'marksheet2',
            'degree3', 'institution3', 'institution_address3', 'cgpa_percentage3', 'marksheet3',
            'degree4', 'institution4', 'institution_address4', 'cgpa_percentage4', 'marksheet4',
            'attached_documents_declaration', 'attested_copies_marksheets', 'name_attestation_officer', 'rank_attestation_officer'
        ]
        labels = {
            'attached_documents_declaration': 'I understand that if any of the documents attached by me are found to be false or fraudulent, my application will be rejected and I will be disqualified from applying to DTU',
            'attested_copies_marksheets': 'Certified that I have attached the attested photocopies of the marksheets/grade-sheets in support of the above stated academic qualifications',
            'name_attestation_officer': 'Name of the Attestation Officer',
            'rank_attestation_officer': 'Position/ Rank of the Attestation Officer'
        }


class FormWorkExperience(ModelForm):
    class Meta:
        model = WorkExperience
        fields = [
            'company_name1', 'company_address1', 'company_position1', 'employment_period1',
            'company_name2', 'company_address2', 'company_position2', 'employment_period2',
        ]


class FormTestScore(ModelForm):
    class Meta:
        model = TestScore
        fields = [
            'test_name1', 'test_score1', 'test_max_score1', 'test_date1',
            'test_name2', 'test_score2', 'test_max_score2', 'test_date2',
            'test_name3', 'test_score3', 'test_max_score3', 'test_date3',
        ]
        widgets = {
            'test_date1': forms.DateInput(attrs={'class': 'datepicker'}),
            'test_date2': forms.DateInput(attrs={'class': 'datepicker'}),
            'test_date3': forms.DateInput(attrs={'class': 'datepicker'}),
        }


class FormDisability(ModelForm):
    class Meta:
        model = Disability
        fields = ['are_you_healthy', 'other_disabilities', 'convicted_any_crime']
        labels = {
            'other_disabilities': 'Health issues we should be aware of',
            'are_you_healthy': 'By agreeing you declare that you are fit and healthy to enroll in our university',
            'convicted_any_crime': 'By agreeing you declare that you are not presently engaged or convicted of any crimes'
        }


class FormScholarship(ModelForm):
    class Meta:
        model = Scholarship
        fields = ['scholarship_bool', 'scholarship_name', 'approval_status', 'approval_letter']
        labels = {
            'scholarship_bool': 'Have you applied for a scholarship/sponsorship from any organisation in your country to support your studies?'
        }
        widgets = {
            'scholarship_bool': forms.RadioSelect
        }


class FormSponsorInfo(ModelForm):
    class Meta:
        model = SponsorInfo
        fields = ['sponsor_name', 'sponsor_relation',
                  'sponsor_bank_statement']  # TODO: Bank statement is a file field check for it
        labels = {
            'sponsor_name': 'Please mention the name of person/organization who will be sponsoring your studies and stay in India for the duration of the course applied for:',
            'sponsor_relation': 'Relation of the person with you (Parent, Spouse, etc.) '
        }


class FormGuardianInfo(ModelForm):
    class Meta:
        model = LocalGuardianInfo
        fields = ['guardian_available', 'guardian_name', 'guardian_address', 'guardian_email', 'guardian_phone']
        labels = {
            'guardian_available':'Do you have a local guardian in India?',
            'guardian_name': 'Name of the local guardian: ',
            'guardian_address': 'Address of the local guardian: ',
            'guardian_email': 'E-Mail',
            'guardian_phone': 'Phone Number'
        }
        widgets = {
            'guardian_available': forms.RadioSelect
        }


class FormRecommendation(ModelForm):
    class Meta:
        model = Recommendations
        fields = [
            'recommendation_name1', 'recommendation_email1', 'recommendation_phone1', 'recommendation_letter1',
            'recommendation_name2', 'recommendation_email2', 'recommendation_phone2', 'recommendation_letter2'
        ]


class FormFinalDeclare(ModelForm):
    class Meta:
        model = FinalDeclaration
        fields = ['declaration', 'signature', 'place', ] # TODO: 'fill_date' not specified because it is non editable field error
        labels = {
            'declaration': 'I agree to all of the statements mentioned in form'
        }
        widgets = {
            'declaration': forms.RadioSelect
        }