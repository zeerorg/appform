# -*- coding: utf-8 -*-
# Generated by Django 1.11.1 on 2017-06-26 08:14
from __future__ import unicode_literals

import app.models
import django.core.validators
from django.db import migrations, models
import django_countries.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='AcademicQualification',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('degree1', models.CharField(max_length=20)),
                ('institution1', models.CharField(max_length=100)),
                ('institution_address1', models.CharField(max_length=100)),
                ('cgpa_percentage1', models.CharField(max_length=5)),
                ('marksheet1', models.FileField(upload_to=app.models.get_file_path_marksheets)),
                ('degree2', models.CharField(blank=True, max_length=20, null=True)),
                ('institution2', models.CharField(blank=True, max_length=100, null=True)),
                ('institution_address2', models.CharField(blank=True, max_length=100, null=True)),
                ('cgpa_percentage2', models.CharField(blank=True, max_length=5, null=True)),
                ('marksheet2', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_marksheets)),
                ('degree3', models.CharField(blank=True, max_length=20, null=True)),
                ('institution3', models.CharField(blank=True, max_length=100, null=True)),
                ('institution_address3', models.CharField(blank=True, max_length=100, null=True)),
                ('cgpa_percentage3', models.CharField(blank=True, max_length=5, null=True)),
                ('marksheet3', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_marksheets)),
                ('degree4', models.CharField(blank=True, max_length=20, null=True)),
                ('institution4', models.CharField(blank=True, max_length=100, null=True)),
                ('institution_address4', models.CharField(blank=True, max_length=100, null=True)),
                ('cgpa_percentage4', models.CharField(blank=True, max_length=5, null=True)),
                ('marksheet4', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_marksheets)),
                ('attached_documents_declaration', models.BooleanField(default=False)),
                ('attested_copies_marksheets', models.BooleanField(default=False)),
                ('name_attestation_officer', models.CharField(max_length=150)),
                ('rank_attestation_officer', models.CharField(max_length=50)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='ApplicationInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('program_applying_for', models.CharField(choices=[(b'btech', b'B.Tech.'), (b'mtech', b'M.Tech.'), (b'phd', b'Ph.D'), (b'mba', b'M.B.A')], max_length=5)),
                ('department_preference_1', models.CharField(blank=True, choices=[(b'B_BT', b'Bio-Technology'), (b'B_COE', b'Computer Engineering'), (b'B_CE', b'Civil Engineering'), (b'B_ECE', b'Electronics and Communication Engineering'), (b'B_EE', b'Electrical Engineering'), (b'B_IT', b'Information Technology'), (b'B_ME', b'Mechanical Engineering'), (b'B_PIE', b'Production and Industrial Engineering'), (b'B_ENE', b'Environmental Engineering'), (b'B_PCT', b'Polymer Science and Chemical Technology'), (b'B_SE', b'Software Engineering'), (b'B_EEE', b'Electrical and Electronics Engineering'), (b'B_MAM', b'Mechanical Engineering with specialization in Automotive'), (b'B_EP', b'Engineering Physics'), (b'B_MC', b'Mathematics and Computing'), (b'M_PTE', b'Polymer Technology'), (b'M_NST', b'Nano Science and Technology'), (b'M_NSE', b'Nuclear Science and Engineering'), (b'M_BIO', b'Bioinformatics'), (b'M_BME', b'Bio Medical Engineering'), (b'M_IBT', b'Industrial Bio Training'), (b'M_GTE', b'Geaotechnical Engineering'), (b'M_HRE', b'Hydraulics and Water Resource Engineering'), (b'M_STE', b'Structural Engineering'), (b'M_CSE', b'Computer Science and Engineering'), (b'M_SWE', b'Software Engineering'), (b'M_ISY', b'Information System'), (b'M_MOCE', b'Microwave and Optical Communication'), (b'M_SPD', b'Signal Processing and Digital Design'), (b'M_VLS', b'VLSI Design and Embedded System'), (b'M_CAI', b'Control and Instrumentation'), (b'M_PSY', b'Power System'), (b'M_ENE', b'Environmental Engineering'), (b'M_CDN', b'Computational Design'), (b'M_PIE', b'Production Engineering'), (b'M_RET', b'Renewable Energy Technology'), (b'M_THE', b'Thermal Engineering')], max_length=6, null=True)),
                ('department_preference_2', models.CharField(blank=True, choices=[(b'B_BT', b'Bio-Technology'), (b'B_COE', b'Computer Engineering'), (b'B_CE', b'Civil Engineering'), (b'B_ECE', b'Electronics and Communication Engineering'), (b'B_EE', b'Electrical Engineering'), (b'B_IT', b'Information Technology'), (b'B_ME', b'Mechanical Engineering'), (b'B_PIE', b'Production and Industrial Engineering'), (b'B_ENE', b'Environmental Engineering'), (b'B_PCT', b'Polymer Science and Chemical Technology'), (b'B_SE', b'Software Engineering'), (b'B_EEE', b'Electrical and Electronics Engineering'), (b'B_MAM', b'Mechanical Engineering with specialization in Automotive'), (b'B_EP', b'Engineering Physics'), (b'B_MC', b'Mathematics and Computing'), (b'M_PTE', b'Polymer Technology'), (b'M_NST', b'Nano Science and Technology'), (b'M_NSE', b'Nuclear Science and Engineering'), (b'M_BIO', b'Bioinformatics'), (b'M_BME', b'Bio Medical Engineering'), (b'M_IBT', b'Industrial Bio Training'), (b'M_GTE', b'Geaotechnical Engineering'), (b'M_HRE', b'Hydraulics and Water Resource Engineering'), (b'M_STE', b'Structural Engineering'), (b'M_CSE', b'Computer Science and Engineering'), (b'M_SWE', b'Software Engineering'), (b'M_ISY', b'Information System'), (b'M_MOCE', b'Microwave and Optical Communication'), (b'M_SPD', b'Signal Processing and Digital Design'), (b'M_VLS', b'VLSI Design and Embedded System'), (b'M_CAI', b'Control and Instrumentation'), (b'M_PSY', b'Power System'), (b'M_ENE', b'Environmental Engineering'), (b'M_CDN', b'Computational Design'), (b'M_PIE', b'Production Engineering'), (b'M_RET', b'Renewable Energy Technology'), (b'M_THE', b'Thermal Engineering')], max_length=6, null=True)),
                ('department_preference_3', models.CharField(blank=True, choices=[(b'B_BT', b'Bio-Technology'), (b'B_COE', b'Computer Engineering'), (b'B_CE', b'Civil Engineering'), (b'B_ECE', b'Electronics and Communication Engineering'), (b'B_EE', b'Electrical Engineering'), (b'B_IT', b'Information Technology'), (b'B_ME', b'Mechanical Engineering'), (b'B_PIE', b'Production and Industrial Engineering'), (b'B_ENE', b'Environmental Engineering'), (b'B_PCT', b'Polymer Science and Chemical Technology'), (b'B_SE', b'Software Engineering'), (b'B_EEE', b'Electrical and Electronics Engineering'), (b'B_MAM', b'Mechanical Engineering with specialization in Automotive'), (b'B_EP', b'Engineering Physics'), (b'B_MC', b'Mathematics and Computing'), (b'M_PTE', b'Polymer Technology'), (b'M_NST', b'Nano Science and Technology'), (b'M_NSE', b'Nuclear Science and Engineering'), (b'M_BIO', b'Bioinformatics'), (b'M_BME', b'Bio Medical Engineering'), (b'M_IBT', b'Industrial Bio Training'), (b'M_GTE', b'Geaotechnical Engineering'), (b'M_HRE', b'Hydraulics and Water Resource Engineering'), (b'M_STE', b'Structural Engineering'), (b'M_CSE', b'Computer Science and Engineering'), (b'M_SWE', b'Software Engineering'), (b'M_ISY', b'Information System'), (b'M_MOCE', b'Microwave and Optical Communication'), (b'M_SPD', b'Signal Processing and Digital Design'), (b'M_VLS', b'VLSI Design and Embedded System'), (b'M_CAI', b'Control and Instrumentation'), (b'M_PSY', b'Power System'), (b'M_ENE', b'Environmental Engineering'), (b'M_CDN', b'Computational Design'), (b'M_PIE', b'Production Engineering'), (b'M_RET', b'Renewable Energy Technology'), (b'M_THE', b'Thermal Engineering')], max_length=6, null=True)),
                ('department_preference_4', models.CharField(blank=True, choices=[(b'B_BT', b'Bio-Technology'), (b'B_COE', b'Computer Engineering'), (b'B_CE', b'Civil Engineering'), (b'B_ECE', b'Electronics and Communication Engineering'), (b'B_EE', b'Electrical Engineering'), (b'B_IT', b'Information Technology'), (b'B_ME', b'Mechanical Engineering'), (b'B_PIE', b'Production and Industrial Engineering'), (b'B_ENE', b'Environmental Engineering'), (b'B_PCT', b'Polymer Science and Chemical Technology'), (b'B_SE', b'Software Engineering'), (b'B_EEE', b'Electrical and Electronics Engineering'), (b'B_MAM', b'Mechanical Engineering with specialization in Automotive'), (b'B_EP', b'Engineering Physics'), (b'B_MC', b'Mathematics and Computing'), (b'M_PTE', b'Polymer Technology'), (b'M_NST', b'Nano Science and Technology'), (b'M_NSE', b'Nuclear Science and Engineering'), (b'M_BIO', b'Bioinformatics'), (b'M_BME', b'Bio Medical Engineering'), (b'M_IBT', b'Industrial Bio Training'), (b'M_GTE', b'Geaotechnical Engineering'), (b'M_HRE', b'Hydraulics and Water Resource Engineering'), (b'M_STE', b'Structural Engineering'), (b'M_CSE', b'Computer Science and Engineering'), (b'M_SWE', b'Software Engineering'), (b'M_ISY', b'Information System'), (b'M_MOCE', b'Microwave and Optical Communication'), (b'M_SPD', b'Signal Processing and Digital Design'), (b'M_VLS', b'VLSI Design and Embedded System'), (b'M_CAI', b'Control and Instrumentation'), (b'M_PSY', b'Power System'), (b'M_ENE', b'Environmental Engineering'), (b'M_CDN', b'Computational Design'), (b'M_PIE', b'Production Engineering'), (b'M_RET', b'Renewable Energy Technology'), (b'M_THE', b'Thermal Engineering')], max_length=6, null=True)),
                ('phd_interest', models.CharField(blank=True, max_length=50, null=True)),
                ('published_papers', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_published_papers)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='BaseModel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='BasicInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('first_name', models.CharField(max_length=50)),
                ('middle_name', models.CharField(blank=True, max_length=50, null=True)),
                ('last_name', models.CharField(max_length=50)),
                ('gender', models.CharField(choices=[(b'M', b'Male'), (b'F', b'Female')], max_length=1)),
                ('date_of_birth', models.DateField()),
                ('parent_first_name', models.CharField(max_length=50)),
                ('parent_middle_name', models.CharField(blank=True, max_length=50, null=True)),
                ('parent_last_name', models.CharField(max_length=50)),
                ('country_of_citizenship', django_countries.fields.CountryField(max_length=2)),
                ('country_of_birth', django_countries.fields.CountryField(max_length=2)),
                ('ethnic_group', models.CharField(choices=[(b'hispanic', b'Hispanic'), (b'white', b'White'), (b'black', b'Black'), (b'asian', b'Asian (Non-Indian)'), (b'africa_american', b'African American'), (b'african', b'African'), (b'alaska', b'Alaska Native'), (b'asian_indian', b'Asian Indian'), (b'hawaaiian', b'Native Hawaaiian'), (b'pascific', b'Other Pascific Islander')], max_length=20)),
                ('phone_number', models.CharField(max_length=15, validators=[django.core.validators.RegexValidator(message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex=b'^\\+?1?\\d{9,15}$')])),
                ('email', models.EmailField(max_length=254)),
                ('permanent_address', models.TextField()),
                ('current_address', models.TextField()),
                ('username', models.CharField(max_length=50, unique=True)),
                ('user_pic', models.ImageField(upload_to=app.models.get_file_path_pic)),
            ],
        ),
        migrations.CreateModel(
            name='Disability',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('are_you_healthy', models.BooleanField(default=True)),
                ('other_disabilities', models.TextField(blank=True, null=True)),
                ('convicted_any_crime', models.BooleanField(default=True)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='FinalDeclaration',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('declaration', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('signature', models.FileField(upload_to=app.models.get_file_path_signature)),
                ('place', models.CharField(max_length=100)),
                ('fill_date', models.DateField(auto_now=True)),
                ('username', models.CharField(max_length=50)),
            ],
        ),
        migrations.CreateModel(
            name='LanguageProficiency',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=50, unique=True)),
                ('proficient_english', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('english_document', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_english_docs)),
                ('toefl_choice', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('toefl_score', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(120), django.core.validators.MinValueValidator(0)])),
                ('ielts_choice', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('ielts_score', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(9), django.core.validators.MinValueValidator(0)])),
                ('other_languages_known', models.CharField(blank=True, max_length=200, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='LocalGuardianInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guardian_available', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('guardian_name', models.CharField(blank=True, max_length=150, null=True)),
                ('guardian_address', models.TextField(blank=True, null=True)),
                ('guardian_email', models.EmailField(blank=True, max_length=254, null=True)),
                ('guardian_phone', models.CharField(blank=True, max_length=15, null=True, validators=[django.core.validators.RegexValidator(message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex=b'^\\+?1?\\d{9,15}$')])),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='PassportInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('passport_number', models.CharField(max_length=10)),
                ('place_of_issue', models.CharField(max_length=20)),
                ('issuing_authority', models.CharField(max_length=20)),
                ('date_of_issue', models.DateField()),
                ('passport_expiry_date', models.DateField()),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='PaymentInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('unique_code', models.CharField(max_length=20)),
                ('bank_name', models.CharField(max_length=50)),
                ('transaction_id', models.CharField(max_length=20)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Recommendations',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('recommendation_name1', models.CharField(blank=True, max_length=150, null=True)),
                ('recommendation_email1', models.EmailField(blank=True, max_length=254, null=True)),
                ('recommendation_phone1', models.CharField(blank=True, max_length=15, null=True, validators=[django.core.validators.RegexValidator(message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex=b'^\\+?1?\\d{9,15}$')])),
                ('recommendation_letter1', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_recommendation)),
                ('recommendation_name2', models.CharField(blank=True, max_length=150, null=True)),
                ('recommendation_email2', models.EmailField(blank=True, max_length=254, null=True)),
                ('recommendation_phone2', models.CharField(blank=True, max_length=15, null=True, validators=[django.core.validators.RegexValidator(message=b"Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.", regex=b'^\\+?1?\\d{9,15}$')])),
                ('recommendation_letter2', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_recommendation)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Scholarship',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('scholarship_bool', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('scholarship_name', models.CharField(blank=True, max_length=100, null=True)),
                ('approval_status', models.CharField(blank=True, max_length=20, null=True)),
                ('approval_letter', models.FileField(blank=True, null=True, upload_to=app.models.get_file_path_scholarship)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='SponsorInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('sponsor_name', models.CharField(max_length=150)),
                ('sponsor_relation', models.CharField(max_length=50)),
                ('sponsor_bank_statement', models.FileField(upload_to=app.models.get_file_path_sponsor)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='TestScore',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('test_name1', models.CharField(blank=True, max_length=20, null=True)),
                ('test_score1', models.CharField(blank=True, max_length=5, null=True)),
                ('test_max_score1', models.CharField(blank=True, max_length=5, null=True)),
                ('test_date1', models.DateField(blank=True, null=True)),
                ('test_name2', models.CharField(blank=True, max_length=20, null=True)),
                ('test_score2', models.CharField(blank=True, max_length=5, null=True)),
                ('test_max_score2', models.CharField(blank=True, max_length=5, null=True)),
                ('test_date2', models.DateField(blank=True, null=True)),
                ('test_name3', models.CharField(blank=True, max_length=20, null=True)),
                ('test_score3', models.CharField(blank=True, max_length=5, null=True)),
                ('test_max_score3', models.CharField(blank=True, max_length=5, null=True)),
                ('test_date3', models.DateField(blank=True, null=True)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='VisaInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('applied_for_indian_visa', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('visa_type', models.CharField(blank=True, max_length=20, null=True)),
                ('visa_expiry_date', models.DateField(blank=True, null=True)),
                ('immigration_office', models.CharField(blank=True, max_length=20, null=True)),
                ('refused_entry', models.BooleanField(choices=[(True, b'Yes'), (False, b'No')], default=False)),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='WorkExperience',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('company_name1', models.CharField(blank=True, max_length=150, null=True)),
                ('company_address1', models.CharField(blank=True, max_length=250, null=True)),
                ('company_position1', models.CharField(blank=True, max_length=100, null=True)),
                ('employment_period1', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(1)])),
                ('company_name2', models.CharField(blank=True, max_length=150, null=True)),
                ('company_address2', models.CharField(blank=True, max_length=250, null=True)),
                ('company_position2', models.CharField(blank=True, max_length=100, null=True)),
                ('employment_period2', models.IntegerField(blank=True, null=True, validators=[django.core.validators.MinValueValidator(1)])),
                ('username', models.CharField(max_length=50, unique=True)),
            ],
        ),
    ]
