import os
import uuid

from django.db import models
from django.core.validators import RegexValidator, MaxValueValidator, MinValueValidator
from django_countries.fields import CountryField

# Create your models here.
GENDER_CHOICES = (
    ('M', 'Male'),
    ('F', 'Female')
)

PROGRAM_CHOICES = (
    ('btech', 'B.Tech.'),
    ('mtech', 'M.Tech.'),
    ('phd', 'Ph.D'),
    ('mba', 'M.B.A')
)

DEPARTMENT_CHOICES = (
    ('B_BT', 'Bio-Technology'),
    ('B_COE', 'Computer Engineering'),
    ('B_CE', 'Civil Engineering'),
    ('B_ECE', 'Electronics and Communication Engineering'),
    ('B_EE', 'Electrical Engineering'),
    ('B_IT', 'Information Technology'),
    ('B_ME', 'Mechanical Engineering'),
    ('B_PIE', 'Production and Industrial Engineering'),
    ('B_ENE', 'Environmental Engineering'),
    ('B_PCT', 'Polymer Science and Chemical Technology'),
    ('B_SE', 'Software Engineering'),
    ('B_EEE', 'Electrical and Electronics Engineering'),
    ('B_MAM', 'Mechanical Engineering with specialization in Automotive'),
    ('B_EP', 'Engineering Physics'),
    ('B_MC', 'Mathematics and Computing'),
    ('M_PTE', 'Polymer Technology'),
    ('M_NST', 'Nano Science and Technology'),
    ('M_NSE', 'Nuclear Science and Engineering'),
    ('M_BIO', 'Bioinformatics'),
    ('M_BME', 'Bio Medical Engineering'),
    ('M_IBT', 'Industrial Bio Training'),
    ('M_GTE', 'Geaotechnical Engineering'),
    ('M_HRE', 'Hydraulics and Water Resource Engineering'),
    ('M_STE', 'Structural Engineering'),
    ('M_CSE', 'Computer Science and Engineering'),
    ('M_SWE', 'Software Engineering'),
    ('M_ISY', 'Information System'),
    ('M_MOCE', 'Microwave and Optical Communication'),
    ('M_SPD', 'Signal Processing and Digital Design'),
    ('M_VLS', 'VLSI Design and Embedded System'),
    ('M_CAI', 'Control and Instrumentation'),
    ('M_PSY', 'Power System'),
    ('M_ENE', 'Environmental Engineering'),
    ('M_CDN', 'Computational Design'),
    ('M_PIE', 'Production Engineering'),
    ('M_RET', 'Renewable Energy Technology'),
    ('M_THE', 'Thermal Engineering')
)

ETHNIC_CHOICES = (
    ('hispanic', 'Hispanic'),
    ('white', 'White'),
    ('black', 'Black'),
    ('asian', 'Asian (Non-Indian)'),
    ('africa_american', 'African American'),
    ('african', 'African'),
    ('alaska', 'Alaska Native'),
    ('asian_indian', 'Asian Indian'),
    ('hawaaiian', 'Native Hawaaiian'),
    ('pascific', 'Other Pascific Islander')
)
PHONE_REGEX = RegexValidator(regex=r'^\+?1?\d{9,15}$', message="Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed.")
BOOL_CHOICES = ((True, 'Yes'), (False, 'No'))


def get_file_name(filename, folder):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join('uploads/'+folder, filename)


def get_file_path_english_docs(instance, filename):
    return get_file_name(filename, 'english_docs')


def get_file_path_marksheets(instance, filename):
    return get_file_name(filename, 'marksheets')


def get_file_path_sponsor(instance , filename):
    return get_file_name(filename, 'sponsor')


def get_file_path_scholarship(instance, filename):
    return get_file_name(filename, 'scholarship')


def get_file_path_signature(instance, filename):
    return get_file_name(filename, 'signature')


def get_file_path_pic(instance, filename):
    return get_file_name(filename, 'pic')


def get_file_path_recommendation(instance, filename):
    return get_file_name(filename, 'recommendation')


def get_file_path_published_papers(instance, filename):
    return get_file_name(filename, 'published_papers')


class BaseModel(models.Model):
    username = models.CharField(max_length=50, null=False, unique=True)


class BasicInfo(models.Model):
    first_name = models.CharField(max_length=50)
    middle_name = models.CharField(max_length=50, null=True, blank=True)
    last_name =  models.CharField(max_length=50)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES)
    date_of_birth = models.DateField(auto_now=False, auto_now_add=False)
    parent_first_name = models.CharField(max_length=50)
    parent_middle_name = models.CharField(max_length=50, null=True, blank=True)
    parent_last_name =  models.CharField(max_length=50)
    country_of_citizenship = CountryField()
    country_of_birth = CountryField()
    ethnic_group = models.CharField(max_length=20, choices=ETHNIC_CHOICES)
    phone_number = models.CharField(validators=[PHONE_REGEX], max_length=15) # validators should be a list
    email = models.EmailField()
    permanent_address = models.TextField()
    current_address = models.TextField()
    username = models.CharField(max_length=50, null=False, unique=True)
    user_pic = models.ImageField(upload_to=get_file_path_pic)


class PassportInfo(models.Model):
    passport_number = models.CharField(max_length=10)
    place_of_issue = models.CharField(max_length=20)
    issuing_authority = models.CharField(max_length=20)
    date_of_issue = models.DateField(auto_now=False, auto_now_add=False)
    passport_expiry_date = models.DateField(auto_now=False, auto_now_add=False)
    username = models.CharField(max_length=50, null=False, unique=True)


class VisaInfo(models.Model):
    applied_for_indian_visa = models.BooleanField(default=False, choices=BOOL_CHOICES)
    visa_type = models.CharField(max_length=20, null=True, blank=True)
    visa_expiry_date = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    immigration_office = models.CharField(max_length=20, null=True, blank=True)
    refused_entry = models.BooleanField(default=False, choices=BOOL_CHOICES)
    username = models.CharField(max_length=50, null=False, unique=True)


class ApplicationInfo(models.Model):
    program_applying_for = models.CharField(max_length=5, choices=PROGRAM_CHOICES)
    department_preference_1 = models.CharField(max_length=6, choices=DEPARTMENT_CHOICES, null=True, blank=True)
    department_preference_2 = models.CharField(max_length=6, choices=DEPARTMENT_CHOICES, null=True, blank=True)
    department_preference_3 = models.CharField(max_length=6, choices=DEPARTMENT_CHOICES, null=True, blank=True)
    department_preference_4 = models.CharField(max_length=6, choices=DEPARTMENT_CHOICES, null=True, blank=True)
    phd_interest = models.CharField(max_length=50, null=True, blank=True)
    published_papers = models.FileField(upload_to=get_file_path_published_papers, null=True, blank=True)
    username = models.CharField(max_length=50, null=False, unique=True)


class LanguageProficiency(models.Model):
    username = models.CharField(max_length=50, null=False, unique=True)
    proficient_english = models.BooleanField(default=False, choices=BOOL_CHOICES)
    english_document = models.FileField(upload_to=get_file_path_english_docs, null=True, blank=True)
    toefl_choice = models.BooleanField(default=False, choices=BOOL_CHOICES)
    toefl_score = models.IntegerField(validators=[MaxValueValidator(120), MinValueValidator(0)], null=True, blank=True)
    ielts_choice = models.BooleanField(default=False, choices=BOOL_CHOICES)
    ielts_score = models.IntegerField(validators=[MaxValueValidator(9), MinValueValidator(0)], null=True, blank=True)
    other_languages_known = models.CharField(max_length=200, null=True, blank=True)


class AcademicQualification(models.Model):
    degree1 = models.CharField(max_length=20)
    institution1 = models.CharField(max_length=100)
    institution_address1 = models.CharField(max_length=100)
    cgpa_percentage1 = models.CharField(max_length=5)
    marksheet1 = models.FileField(upload_to=get_file_path_marksheets)

    degree2 = models.CharField(max_length=20, null=True, blank=True)
    institution2 = models.CharField(max_length=100, null=True, blank=True)
    institution_address2 = models.CharField(max_length=100, null=True, blank=True)
    cgpa_percentage2 = models.CharField(max_length=5, null=True, blank=True)
    marksheet2 = models.FileField(upload_to=get_file_path_marksheets, null=True, blank=True)

    degree3 = models.CharField(max_length=20, null=True, blank=True)
    institution3 = models.CharField(max_length=100, null=True, blank=True)
    institution_address3 = models.CharField(max_length=100, null=True, blank=True)
    cgpa_percentage3 = models.CharField(max_length=5, null=True, blank=True)
    marksheet3 = models.FileField(upload_to=get_file_path_marksheets, null=True, blank=True)

    degree4 = models.CharField(max_length=20, null=True, blank=True)
    institution4 = models.CharField(max_length=100, null=True, blank=True)
    institution_address4 = models.CharField(max_length=100, null=True, blank=True)
    cgpa_percentage4 = models.CharField(max_length=5, null=True, blank=True)
    marksheet4 = models.FileField(upload_to=get_file_path_marksheets, null=True, blank=True)

    attached_documents_declaration = models.BooleanField(default=False)
    attested_copies_marksheets = models.BooleanField(default=False)
    name_attestation_officer = models.CharField(max_length=150)
    rank_attestation_officer = models.CharField(max_length=50)

    username = models.CharField(max_length=50, null=False, unique=True)


class WorkExperience(models.Model):
    company_name1 = models.CharField(max_length=150, null=True, blank=True)
    company_address1 = models.CharField(max_length=250, null=True, blank=True)
    company_position1 = models.CharField(max_length=100, null=True, blank=True)
    employment_period1 = models.IntegerField(validators=[MinValueValidator(1)], null=True, blank=True)   # in months

    company_name2 = models.CharField(max_length=150, null=True, blank=True)
    company_address2 = models.CharField(max_length=250, null=True, blank=True)
    company_position2 = models.CharField(max_length=100, null=True, blank=True)
    employment_period2 = models.IntegerField(validators=[MinValueValidator(1)], null=True, blank=True)   # in months

    username = models.CharField(max_length=50, null=False, unique=True)


class TestScore(models.Model):
    test_name1 = models.CharField(null=True, blank=True, max_length=20)
    test_score1 = models.CharField(null=True, blank=True, max_length=5)
    test_max_score1 = models.CharField(null=True, blank=True, max_length=5)
    test_date1 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)

    test_name2 = models.CharField(null=True, blank=True, max_length=20)
    test_score2 = models.CharField(null=True, blank=True, max_length=5)
    test_max_score2 = models.CharField(null=True, blank=True, max_length=5)
    test_date2 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)

    test_name3 = models.CharField(null=True, blank=True, max_length=20)
    test_score3 = models.CharField(null=True, blank=True, max_length=5)
    test_max_score3 = models.CharField(null=True, blank=True, max_length=5)
    test_date3 = models.DateField(auto_now=False, auto_now_add=False, null=True, blank=True)
    username = models.CharField(max_length=50, null=False, unique=True)


class Disability(models.Model):
    are_you_healthy = models.BooleanField(default=True)
    other_disabilities = models.TextField(null=True, blank=True)

    convicted_any_crime = models.BooleanField(default=True)
    username = models.CharField(max_length=50, null=False, unique=True)


class Scholarship(models.Model):
    scholarship_bool = models.BooleanField(choices=BOOL_CHOICES, default=False)
    scholarship_name = models.CharField(max_length=100, null=True, blank=True)
    approval_status = models.CharField(max_length=20, null=True, blank=True)
    approval_letter = models.FileField(null=True, blank=True, upload_to=get_file_path_scholarship)
    username = models.CharField(max_length=50, null=False, unique=True)


class SponsorInfo(models.Model):
    sponsor_name = models.CharField(max_length=150)
    sponsor_relation = models.CharField(max_length=50)
    sponsor_bank_statement = models.FileField(upload_to=get_file_path_sponsor)
    username = models.CharField(max_length=50, null=False, unique=True)


class LocalGuardianInfo(models.Model):
    guardian_available = models.BooleanField(choices=BOOL_CHOICES, default=False)
    guardian_name = models.CharField(max_length=150, null=True, blank=True)
    guardian_address = models.TextField(null=True, blank=True)
    guardian_email = models.EmailField(null=True, blank=True)
    guardian_phone = models.CharField(validators=[PHONE_REGEX], max_length=15, null=True, blank=True)
    username = models.CharField(max_length=50, null=False, unique=True)


class Recommendations(models.Model):
    recommendation_name1 = models.CharField(max_length=150, null=True, blank=True)
    recommendation_email1 = models.EmailField(null=True, blank=True)
    recommendation_phone1 = models.CharField(validators=[PHONE_REGEX], max_length=15, null=True, blank=True)
    recommendation_letter1 = models.FileField(upload_to=get_file_path_recommendation, null=True, blank=True)

    recommendation_name2 = models.CharField(max_length=150, null=True, blank=True)
    recommendation_email2 = models.EmailField(null=True, blank=True)
    recommendation_phone2 = models.CharField(validators=[PHONE_REGEX], max_length=15, null=True, blank=True)
    recommendation_letter2 = models.FileField(upload_to=get_file_path_recommendation, null=True, blank=True)

    username = models.CharField(max_length=50, null=False, unique=True)


class PaymentInfo(models.Model):
    bank_name = models.CharField(max_length=50)
    transaction_id = models.CharField(max_length=50)
    order_id = models.CharField(max_length=37)
    bank_txn_id = models.CharField(max_length=50)
    txn_amount = models.DecimalField(decimal_places=2, max_digits=8)
    username = models.CharField(max_length=50, null=False, unique=True)

    #passport = models.FileField(null=True, blank=True, upload_to='admitform/passports')
    #statement_purpose = models.FileField(upload_to='admitform/sop')
    #resume = models.FileField(upload_to='admitform/resume')


class FinalDeclaration(models.Model):
    declaration = models.BooleanField(choices=BOOL_CHOICES, default=False)
    signature = models.FileField(upload_to=get_file_path_signature)
    place = models.CharField(max_length=100)
    fill_date = models.DateField(auto_now=True, auto_now_add=False)
    username = models.CharField(max_length=50)
    # def __str__(self):
    #     return self.first_name + self.last_name
    #
    # def check_not_null(self):
    #     if (self.signature and self.place and self.fill_date and self.unique_code and
    #         self.bank_name and self.transaction_id and self.sponsor_name and self.sponsor_relation and
    #         self.sponsor_bank_statement and self.name_attestation_officer and self.rank_attestation_officer and
    #         self.degree1 and self.institution1 and self.cgpa_percentage1 and self.marksheet1 and
    #         self.program_applying_for and self.department_preference_1 and self.passport_number and self.place_of_issue and
    #         self.issuing_authority and self.date_of_issue and self.passport_expiry_date):
    #         return True
    #     else:
    #         return False


class PaymentIntermediate(models.Model):
    username = models.CharField(max_length=50, null=False, unique=True)
    order_id = models.CharField(max_length=37)
    txn_amount = models.DecimalField(decimal_places=2, max_digits=8)
    completed = models.BooleanField(default=False)
