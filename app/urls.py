from django.conf.urls import url, include
from django.contrib import admin
from app import views as core_views
from django.contrib.auth import views as auth_views

from .views import *

urlpatterns = [
    url(r'^$', get_form, name='get_form'),
    url(r'^login/$', core_views.login_view, name='login'),
    url(r'^logout/$', core_views.logout_view, name='logout'),
    url(r'^signup/$', core_views.signup, name='signup'),
    url(r'^get_csv/$', core_views.get_csv, name='get_csv'),
    url(r'^get_pdf/$', core_views.get_pdf, name='get_pdf'),
    url(r'^admin-get-pdf/$', core_views.get_pdf_admin, name="get_pdf_admin"),
    url(r'^paytm-get-response-info/$', core_views.get_paytm_response, name="get_paytm_response"),
   url(r'^paytm-get-response-info$', core_views.get_paytm_response, name="get_paytm_response_NO_slash")

]

for x in FORM_MODELS:
    urlpatterns.append(
        url(r'^'+x[0]+'/$', core_views.form_page, {'url': x[0]})
    )
