import io
import zipfile
import re
import requests

from django.contrib.auth.decorators import login_required
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail, EmailMessage
from django.http import HttpResponse
from django.http.response import Http404
from django.db.models import ObjectDoesNotExist
from djqscsv import render_to_csv_response, write_csv

from django.contrib.auth.forms import UserCreationForm
from django.shortcuts import render, redirect

from django.contrib.auth import authenticate, login, logout

from .forms import *
from .models import *
from appform import settings

import Checksum
from appform import mysettings

from django.views.decorators.csrf import csrf_exempt

FORM_MODELS = (
    ('basic-info',      BasicInfo),
    ('passport-info',   PassportInfo),
    ('visa-info',       VisaInfo),
    ('app-info',        ApplicationInfo),
    ('lang-info',       LanguageProficiency),
    ('acad-info',       AcademicQualification),
    ('work-info',       WorkExperience),
    ('test-info',       TestScore),
    ('disability-info', Disability),
    ('scholar-info',    Scholarship),
    ('sponsor-info',    SponsorInfo),
    ('guardian-info',   LocalGuardianInfo),
    ('recommend-info',  Recommendations),
    ('pay-info',        PaymentInfo),
    ('final-declare',   FinalDeclaration)
)

FORMS = {
    'basic-info': FormBasicInfo,
    'passport-info': FormPassportInfo,
    'visa-info': FormVisaInfo,
    'app-info': FormApplicationInfo,
    'lang-info': FormLanguageProficiency,
    'acad-info': FormAcademicQualification,
    'work-info': FormWorkExperience,
    'test-info': FormTestScore,
    'disability-info': FormDisability,
    'scholar-info': FormScholarship,
    'sponsor-info': FormSponsorInfo,
    'guardian-info': FormGuardianInfo,
    'recommend-info': FormRecommendation,
    'final-declare': FormFinalDeclare
}

FORM_PAGE_NUM = {
    'basic-info': 0,
    'passport-info': 1,
    'visa-info': 2,
    'app-info': 3,
    'lang-info': 4,
    'acad-info': 5,
    'work-info': 6,
    'test-info': 7,
    'disability-info': 8,
    'scholar-info': 9,
    'sponsor-info': 10,
    'guardian-info': 11,
    'recommend-info': 12,
    'pay-info': 13,
    'final-declare': 14
}

MAIL_MESSAGE = """
You've signed up for being International Student at DTU
Your username : {0}
Note: Remember your password as it cannot be recovered afterwards.
To continue with your application go to: http://localhost:8000/application

NOTE: This is auto generated mail. Do not reply to this.
"""


def send_email(to, user):
    print("Sending mail to " + to)
    new_mail = EmailMessage('DTU Admission', MAIL_MESSAGE.format(user), to=[to])
    new_mail.send()
    pass


@login_required(login_url='/application/login')
def get_form(request):  # TODO: do something so that you know which form to pick
    return redirect('/application/basic-info')


@login_required(login_url='/application/login')
def basic_info(request):
    #
    # Get the previous object from model
    # If object is not there redirect to previous page
    # Get the object from model and see if exists
    # If object is there redirect to next page
    #
    # No previous object here
    try:
        student_basic_info = BasicInfo.objects.get(username = request.user.username)
        return redirect('/application/passport-info')
    except ObjectDoesNotExist as exception:
        pass

    form = FormBasicInfo(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        basic = form.save(commit=False)
        basic.username = request.user.username
        basic.save()
        #return HttpResponse("Basic Form Saved")
        return redirect('/application/visa-info')
    return render(request, 'forms/basic-form.html', {'form': form})


@login_required(login_url='/application/login')
def passport_info(request):
    try:
        student_basic_info = BasicInfo.objects.get(username = request.user.username)
    except ObjectDoesNotExist as exception:
        return redirect('/application/basic-info')

    try:
        student_basic_info = PassportInfo.objects.get(username = request.user.username)
        return redirect('/application/visa-info')
    except ObjectDoesNotExist as exception:
        pass

    form = FormPassportInfo(request.POST or None)
    if form.is_valid() and request.method == 'POST':
        basic = form.save(commit=False)
        basic.username = request.user.username
        basic.save()
        #return HttpResponse("Basic Form Saved")
        return redirect('/application/visa-info')
    return render(request, 'forms/passport-form.html', {'form': form})


def signup(request):
    if request.user.is_authenticated():
        return redirect('/application')
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('/application')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html', {'form': form})


def login_view(request, error=None):
    if request.user.is_authenticated():
        return redirect('/application')
    username = request.POST.get('username', None)
    password = request.POST.get('password', None)
    user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        # Redirect to a success page.
        return redirect('/application')
    else:
        if username is not None:
            error = "Wrong Username"
        return render(request, 'login.html', {'error': error})


def logout_view(request):
    logout(request)
    return redirect('/application/login')


@login_required(login_url='/application/login')
def form_page(request, url=None):
    if url:
        num = FORM_PAGE_NUM[url]
        if not settings.DEBUG and request.method == 'GET': # run this when it is in production
            prev_num = num
            while True:
                if prev_num > 0:
                    prev_num -= 1
                    try:
                        student_basic_info = FORM_MODELS[prev_num][1].objects.get(username=request.user.username)
                        if prev_num != num - 1:
                            return redirect('/application/' + FORM_MODELS[prev_num][0])
                        break
                    except ObjectDoesNotExist as exception:
                        continue
                else:
                    break

            next_num = num
            while True:
                try:
                    student_basic_info = FORM_MODELS[next_num][1].objects.get(username=request.user.username)

                    if next_num+1 < len(FORM_PAGE_NUM):
                        next_num += 1
                    else:
                        return redirect('/application/get_pdf')

                except ObjectDoesNotExist as exception:
                    if next_num != num:
                        return redirect('/application/' + FORM_MODELS[next_num][0])
                    else:
                        break

        if url == 'pay-info':
            userinfo = BasicInfo.objects.get(username = request.user.username)
            pre_pay = PaymentIntermediate.objects.filter(username=request.user.username).delete()

            amount = getInr(300)
            order_id = uuid.uuid4()
            my_data = {
                'MID': mysettings.MID,
                'ORDER_ID': order_id,
                'CUST_ID': request.user.username,
                'TXN_AMOUNT': amount,
                'CHANNEL_ID': 'WEB',
                'INDUSTRY_TYPE_ID': 'Retail',
                'WEBSITE': mysettings.PAYMENT_WEBSITE,
            }
            my_data['CHECKSUMHASH'] = Checksum.generate_checksum(my_data, mysettings.MERCHANT_KEY)
            my_data['MOBILE_NO'] = userinfo.phone_number[-10:]  # TODO : ASK if this needs to be international but how ?
            my_data['EMAIL'] = userinfo.email
            my_data['CALLBACK_URL'] = mysettings.CALLBACK_URL
            my_data['REQUEST_TYPE'] = 'DEFAULT'
            payment_redirect_url = mysettings.PAYMENT_REDIRECT_URL

            payment_intermediate = PaymentIntermediate()
            payment_intermediate.username = request.user.username
            payment_intermediate.order_id = order_id
            payment_intermediate.txn_amount = amount
            payment_intermediate.completed = False
            payment_intermediate.save()

            return render(request, 'forms/pay-info.html', {'my_data': my_data, 'payment_redirect_url': payment_redirect_url})

        form = FORMS[url](request.POST or None, request.FILES or None)
        if form.is_valid() and request.method == 'POST':
            basic = form.save(commit=False)
            basic.username = request.user.username
            basic.save()
            # return HttpResponse("Basic Form Saved")
            # qs = FORM_MODELS[num][1].objects.all()
            # with open(url+'.csv', 'w') as csv_file:
            #     write_csv(qs, csv_file)

            if url == "basic-info":
                send_email(request.POST["email"], request.user.username)
            if num + 1 < len(FORM_PAGE_NUM):
                return redirect('/application/' + FORM_MODELS[num + 1][0])
            else:
                return redirect('/application/get_pdf')
        return render(request, 'forms/' + url + '.html', {'form': form})

    raise Http404("BAD URL")


def get_csv(request):
    if not request.user.is_staff:
        raise PermissionDenied
    for models_inner in FORM_MODELS:
        with open(models_inner[0] + '.csv', 'w') as csv_file:
            qs = models_inner[1].objects.all()
            write_csv(qs, csv_file)

    zip_io = io.BytesIO()
    with zipfile.ZipFile(zip_io, mode='w', compression=zipfile.ZIP_DEFLATED) as backup_zip:
        for x in FORM_MODELS:
            backup_zip.write(x[0] + '.csv')
    response = HttpResponse(zip_io.getvalue(), content_type='application/x-zip-compressed')
    response['Content-Disposition'] = 'attachment; filename=%s' % 'your_zipfilename' + ".zip"
    response['Content-Length'] = zip_io.tell()
    return response


@login_required(login_url='/application/login')
def get_pdf(request):
    """
    :param request:
    :return: the html view of the whole form
    """
    if not settings.DEBUG:
        try:
            student_basic_info = FORM_MODELS[-1][1].objects.get(username=request.user.username)
        except ObjectDoesNotExist as exception:
            return redirect('/application/' + FORM_MODELS[-1][0])

    return render_pdf(request.user.username, request)
    pass


def get_pdf_admin(request):
    if not request.user.is_staff:
        raise PermissionDenied
    user = request.GET.get('user', None)
    if user:
        return render_pdf(user, request)
    else:
        raise Http404


def render_pdf(username, request):
    class A:
        hey = "yo"

    my_forms = []
    for x in FORM_MODELS:
        try:
            user_model = x[1].objects.get(username=username)
        except:
            user_model = A()
        my_forms.append(
            FORMS[x[0]](user_model.__dict__, user_model.__dict__)
        )
    return render(request, 'my_pdf.html', {'forms': my_forms, 'user': username})


@csrf_exempt
def get_paytm_response(request):
    if request.method == 'POST':
        user_payment_intermediate = PaymentIntermediate.objects.get(username=request.user.username)
        userinfo = BasicInfo.objects.get(username=request.user.username)
        amount = str(user_payment_intermediate.txn_amount)
        my_data = {
            'MID': mysettings.MID,
            'ORDER_ID': user_payment_intermediate.order_id,
            'CUST_ID': request.user.username,
            'TXN_AMOUNT': amount,
            'CHANNEL_ID': 'WEB',
            'INDUSTRY_TYPE_ID': 'Retail',
            'WEBSITE': mysettings.PAYMENT_WEBSITE
        }
        if Checksum.verify_checksum(my_data, mysettings.MERCHANT_KEY, request.POST["CHECKSUMHASH"]):
            response_dict = request.POST
            TXNID = response_dict["TXNID"]
            ORDERID = response_dict["ORDERID"]
            BANKTXNID = response_dict["BANKTXNID"]
            TXNAMOUNT = response_dict["TXNAMOUNT"]
            CURRENCY = response_dict["CURRENCY"]
            BANKNAME = response_dict["BANKNAME"]
            STATUS = response_dict["STATUS"]

            if STATUS == 'TXN_SUCCESS':
                user_payment_intermediate.completed = True
                user_payment_intermediate.save()
                payment_info = PaymentInfo()
                payment_info.order_id = ORDERID
                payment_info.username = request.user.username
                payment_info.bank_name = BANKNAME
                payment_info.transaction_id = TXNID
                payment_info.bank_txn_id = BANKTXNID
                payment_info.txn_amount = TXNAMOUNT
                payment_info.save()
                # CREATE A PaymentInfo object here and save it
                return render(request, 'payment-success.html')
            else:
                return render(request, 'payment-failure.html')
        else:
            raise Http404("Bad " + str(my_data))
    raise Http404("A get request !!")

    pass


def getInr(amount):
    x = requests.get("https://www.google.com/finance/converter?a=" + str(amount) + "&from=USD&to=INR")
    m = re.search('<span class=bld>(.+?)</span>', x.text)
    return float(m.group(0)[16:-11])

# TODO: add Terms and COndition
# Note: Only query for transactions (PaymentIntermediates)
